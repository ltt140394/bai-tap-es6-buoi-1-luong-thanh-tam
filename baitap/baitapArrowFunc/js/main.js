const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];

const renderPickColorButton = () => {
    let contentHTML = "";

    colorList.forEach((color, index) => {
        if (index == 0) {
            let contentBtn = /*html */ `        
            <button class="color-button ${color} active" id="${color}" onclick="changeColorHouse('${color}')"></button>           
        `;
            contentHTML += contentBtn;
        } else {
            let contentBtn = /*html */ `        
            <button class="color-button ${color}" id="${color}" onclick="changeColorHouse('${color}')"></button>           
        `;
            contentHTML += contentBtn;
        }
    });

    document.getElementById("colorContainer").innerHTML = contentHTML;
}

renderPickColorButton();

const changeColorHouse = (color) => {
    document.getElementById("house").setAttribute("class", `house ${color}`);

    colorList.forEach((item) => {
        if (item == color) {
            document.getElementById(`${item}`).setAttribute("class", `color-button ${item} active`);
        };

        if (item != color) {
            document.getElementById(`${item}`).setAttribute("class", `color-button ${item}`);
        };
    });
}




