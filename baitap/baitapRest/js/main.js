/* Hàm làm tròn số tới n số sau phẩy */
function lamTronSoSauPhay(num,n){
    //num : số cần xử lý
    //n: số chữ số sau dấu phẩy cần lấy
    let base = 10**n;
    let result = Math.round(num * base) / base ;
    return result;
}

const tinhDTB = (...scoresList) => {
    let sum = 0;

    scoresList.forEach((scores) => {
        sum += scores;        
    });
    
    let result = sum / (scoresList.length);
    return result;
}

document.getElementById("btnKhoi1").addEventListener("click", () => {
    let diemToan = document.getElementById("inpToan").value * 1;
    let diemLy = document.getElementById("inpLy").value * 1;
    let diemHoa = document.getElementById("inpHoa").value * 1;

    document.getElementById("tbKhoi1").innerText = lamTronSoSauPhay((tinhDTB(diemToan,diemLy,diemHoa)),2);
});

document.getElementById("btnKhoi2").addEventListener("click", () => {
    let diemVan = document.getElementById("inpVan").value * 1;
    let diemSu = document.getElementById("inpSu").value * 1;
    let diemDia = document.getElementById("inpDia").value * 1;
    let diemAnh = document.getElementById("inpEnglish").value * 1;

    document.getElementById("tbKhoi2").innerText = lamTronSoSauPhay((tinhDTB(diemVan,diemSu,diemDia,diemAnh)),2);
});