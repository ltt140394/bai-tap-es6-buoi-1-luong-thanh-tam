const mainText = "HoverMe!";

const characterList = [...mainText];

const renderCharacterList = () => {
    let contentHTML = "";

    characterList.forEach((char, index) => {
        let contentSpan = /*html */ `
        <span id="${index}" onMouseOver="seeMagicNow(${index})" onMouseOut="returnNormal(${index})">${char}</span>
        `;

        contentHTML += contentSpan;
    });

    document.querySelector(".heading").innerHTML = contentHTML;
}

renderCharacterList();

const seeMagicNow = (id) => {
    document.getElementById(id).style.transform = `rotate(${Math.floor(Math.random() * 721) - 360}deg) scale(2)`;
    document.getElementById(id).style.transition = "1s";
}

const returnNormal = (id) => {
    document.getElementById(id).style.transform = "rotate(0deg) scale(1)";
    document.getElementById(id).style.transition = "1s";
}




